<?php
function hitung($string)
{
    if(preg_match('/(\d+)(?:\s*)([\+\-\*\:\%])(?:\s*)(\d+)/', $string, $matches) !== FALSE){
        $operator = $matches[2];
    
        switch($operator){
            case '+':
                $p = $matches[1] + $matches[3];
                break;
            case '-':
                $p = $matches[1] - $matches[3];
                break;
            case '*':
                $p = $matches[1] * $matches[3];
                break;
            case ':':
                $p = $matches[1] / $matches[3];
                break;
            case '%':
                $p = $matches[1] % $matches[3];
                break;
        }
    
        echo $p.'<br>';
    }
}

echo hitung("102*2");
echo hitung("2+3");
echo hitung("100:25");
echo hitung("10%2");
echo hitung("99-2");



?>